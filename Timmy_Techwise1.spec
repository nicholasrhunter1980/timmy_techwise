# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(
    ['Main.py'],
    pathex=[],
    binaries=[],
    datas=[('audio/*.wav','audio'), ('img/background/*.png', 'img/background'), ('img/boss/Death/*.png','img/boss/Death'), ('img/boss/Idle/*.png','img/boss/Idle'), ('img/boss/Jump/*.png','img/boss/Jump'), ('img/boss/Run/*.png','img/boss/Run'), ('img/buttons/*.png','img/buttons'), ('img/enemy/Death/*.png','img/enemy/Death'), ('img/enemy/Idle/*.png','img/enemy/Idle'), ('img/enemy/Jump/*.png','img/enemy/Jump'), ('img/enemy/Run/*.png', 'img/enemy/Run'), ('img/explosion/*.png','img/explosion'), ('img/icons/*.png','img/icons'), ('img/menus/*.png','img/menus'), ('img/player/Death/*.png','img/player/Death'), ('img/player/Idle/*.png','img/player/Idle'), ('img/player/Jump/*.png','img/player/Jump'), ('img/player/Run/*.png','img/player/Run'), ('img/tile/*.png','img/tile'), ('img/*.png','img')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='Timmy_Techwise',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon='icon.ico',
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='Timmy_Techwise',
)
